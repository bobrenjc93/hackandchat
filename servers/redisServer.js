var redis = require('redis'),
    redis_client = redis.createClient(),
    dbLogic = require('./pgLogic');

var months = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];

function getHashHeader() {
  var currentdate = new Date();
  return currentdate.getFullYear() + ':' +
         currentdate.getMonth() + ':' +
         currentdate.getDate() + ':' +
         currentdate.getHours();
}

function getTimeStamp() {
  var currentdate = new Date();
  return months[currentdate.getMonth()] + ' ' +
         currentdate.getDate() + ', ' +
         currentdate.getFullYear() + ' ' +
         ((currentdate.getHours()+1) < 13 ? currentdate.getHours()+1 : currentdate.getHours()+1 - 12) + ':' +
         ((currentdate.getMinutes() < 10) ? '0':'') +
         currentdate.getMinutes() + ' ' + 
         (currentdate.getHours() < 12 ? 'AM': 'PM');
}

exports.modifyCurrentUserList = function(clientInfo) {
  redis_client.sadd("currentUserList", clientInfo);
}

function addUserToDatabase(id, displayname, username, avatar_url, email) {
  redis_client.sismember("userList", id, function(error, result) {
    if (error) console.log('Error: ' + error);
    else if(!result) {
      redis_client.sadd("userList", id);
  //  commented out for development
  //  dbLogic.pushUserDataToDB(id, displayname, username, avatar_url, email);
  //  }
    }
  });
}

exports.pushToHistory = function(message, messageType, name) {
  console.log(getTimeStamp());
  var pushObject = JSON.stringify({ ts : getTimeStamp(), ms : message , type : messageType, userName : name });
  redis_client.lpush(
    getHashHeader(),
    pushObject,
    function(error, result) {
      if (error) console.log('Error: ' + error);
      else console.log('Push to Redis Success : ' + pushObject);
  });
}

exports.loadHistory = function(connID, whisperFunc) {
  redis_client.lrange(
    getHashHeader(),
    0,
    10, //1 for dev purposes
    function(error, result) {
      if (error) console.log('Error: '+ error);
      else {
        for(var i in result)
          whisperFunc(connID, { type: 'history', message: result[i], id: connID });
      }
  })
}

exports.checkClientID = function(id, displayname, username, avatar_url, email) {
  addUserToDatabase(id, displayname, username, avatar_url, email);
}

exports.loadCurrentUserList = function(connID, whisperFunc) {
  redis_client.smembers("currentUserList", function(error, result) {
    if (error) console.log('Error: '+ error);
    console.log(result);
    for(var i in result) {
      whisperFunc(connID, { type: 'sidebar', message: result[i], id: connID });
    }
  });
}

exports.updateCurrentUserList = function(userInfo) {
  redis_client.srem("currentUserList", userInfo);
}

exports.getTS = function() { 
  return getTimeStamp();
}

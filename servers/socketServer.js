var http = require('http'),
    sockjs = require('sockjs'),
    redisServer = require('./redisServer'),
    expressServer = require('./expressServer');

var webSockets,
    server = http.createServer(),
    clients = {},

webSockets = sockjs.createServer();
webSockets.on('connection', onConnection);
webSockets.installHandlers(server, { prefix:'/echo' } );
server.listen(9999, '0.0.0.0');

function onConnectionStartup(connection) {
//  clients[connection.id] = { connID: connection, name: "", id: "" };
}

function onDataCallback(data, connection) {
  data = JSON.parse(data);

  if ( data.type == 'text' && data.message) {
    broadcastMessage(data, connection);
  }

  if (data.type == 'initialization') {
    broadcast({ type: 'newUser', message: data.name, image: data.URL, id: data.id , ts: redisServer.getTS() }, connection.id);
    clients[connection.id] = { connID: connection, name: data.name, id: data.id, imageURL: data.URL };
    redisServer.pushToHistory(data.name + " has connected", "connection", data.name);
    redisServer.loadHistory(connection.id, whisper);
    redisServer.modifyCurrentUserList(JSON.stringify({
      distName : clients[connection.id].name,
      avatarURL : clients[connection.id].imageURL,
      clientID : clients[connection.id].id }));
    redisServer.loadCurrentUserList(connection.id, whisper);
  }
}

function onCloseCallback(connection) {
  broadcast({ type: 'userLeft', message: clients[connection.id].name, ts: redisServer.getTS() }, connection.id);
  broadcast({ type: 'disconnect', message: clients[connection.id].id }, connection.id);
  redisServer.updateCurrentUserList(JSON.stringify({ distName : clients[connection.id].name, avatarURL : clients[connection.id].imageURL }));
  delete clients[connection.id];
}

function broadcastMessage(data, connection) {
  data.message = data.message.substr(0, 128);
  redisServer.pushToHistory(data.message, "message", clients[connection.id].name);
  broadcast({ type: 'message', 
              message: data.message, 
              id: connection.id, 
              name: clients[connection.id].name, 
              ts: redisServer.getTS() });
}

function onConnection(conn) {
  onConnectionStartup(conn);

  conn.on('data', function (data) {
    onDataCallback(data, conn);
  });

  conn.on('close', function onCloseCB () {
    onCloseCallback(conn);
  });
}

function whisper (id, message) {
  if ( clients[id].connID )
    clients[id].connID.write( JSON.stringify(message) );
}

function broadcast (message, exclude) {
  for ( var i in clients ) {
    if ( i != exclude )
      clients[i].connID.write( JSON.stringify(message) );
  }
}

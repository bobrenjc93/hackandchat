var express = require('express'),
    passport = require('passport'),
    redisServer = require('./redisServer'),
    util = require('util'),
    GitHubStrategy = require('passport-github').Strategy;

var app = express(),
    currentUser,
		currentUserImage;

passport.serializeUser(function(user, done) {
    done(null, user);
});

passport.deserializeUser(function(obj, done) {
    done(null, obj);
});

passport.use(new GitHubStrategy({
    clientID: "71acc8889b4855b574cf",
    clientSecret: "51a34fbadaf9739e115dfc49f4d31272d85576a0",
    callbackURL: "http://localhost:3000/auth/github/callback"
  },
  function(accessToken, refreshToken, profile, done) {
    process.nextTick(function () {
      // To keep the example simple, the user's GitHub profile is returned to
      // represent the logged-in user.  In a typical application, you would want
      // to associate the GitHub account with a user record in your database,
      // and return that user instead.
      currentUser = profile.displayName;
      currentUserImage = JSON.parse(profile._raw).avatar_url
      redisServer.checkClientID(profile.id, 
                                profile.displayName, 
                                profile.username, 
                                JSON.parse(profile._raw).avatar_url, 
                                profile.emails[0].value);
      return done(null, profile);
    });
  }
));

app.configure(function() {
  app.set('views', __dirname + '/views');
  app.set('view engine', 'ejs');
  app.use(express.logger());
  app.use(express.cookieParser());
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.session({ secret: 'keyboard cat' }));
  // Initialize Passport!  Also use passport.session() middleware, to support
  // persistent login sessions (recommended).
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});

app.get('/', function(req, res){
  res.render('login', { user: req.user });
});

app.get('/demo', function (req, res) {
  res.render('demo', { user: JSON.stringify(req.user) });
});

app.get('/account', ensureAuthenticated, function(req, res){
  res.render('account', { user: req.user });
});

app.get('/login', function(req, res){
  //shouldn't be a problem
  res.render('login', { user: req.user });
});

app.get('/signout', function(req, res){
  res.render('login', { user: req.user });
});


app.get('/auth/github', passport.authenticate('github'), function(req, res){
//redirect to github should happen
});

app.get('/auth/github/callback',
  passport.authenticate('github', { failureRedirect: '/login' }),
  function(req, res) {
    res.redirect('/demo');
  }
);

app.post("/send", function(req, res, next) {
  for(var id in connections) {
    connections[id].write('received POST');
  }
  res.send({});
});

app.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});

app.listen(3000);

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) { return next(); }
  res.redirect('/login')
}

exports.getEnteredUser = function() {
  return currentUser;
}

$(document).ready(function () {
  //JANK ASS SHIT
  var field = $('#message'),
      chat = $('#chat-body'),
      userList = $('#who-is-online-body'),
      sock = new SockJS('http://127.0.0.1:9999/echo'),
      sockId = false;

  $('#message').keydown(function (e){
    if(e.keyCode == 13){
      sendMessage();
    }
  });

  function sendMessage () {
    if (!field.val().length ) return;
    sock.send(JSON.stringify({
      id: user_id,
      type: 'text',
      message: field.val()
    }));

    field.val('');
  }

  function sendInitPacket () {
    sock.send(JSON.stringify({
      id: user_id,
      name: user_name,
      URL: avatar_url,
      type: 'initialization'
    }));
  }

  sock.onopen = function () {
    sendInitPacket();
  };

  sock.onmessage = function (e) {
    var data = JSON.parse(e.data);

    switch ( data.type ) {
      case 'newUser':
        appendMessage('connected', data.message, data.ts, data.ts);
        addUser(data.message, data.image, data.id);
        break;
      case 'userLeft':
        appendMessage('disconnected', data.message, data.ts, data.ts);
        break;
      case 'message':
        appendMessage('message', data.message, data.name, data.ts);
        break;
      case 'history':
        appendMessage('history', JSON.parse(data.message), JSON.parse(data.message).userName);
        break;
      case 'sidebar':
        updateConnectedUsers(JSON.parse(data.message),'add');
        break;
      case 'disconnect':
        removeUser(data.message);
        break;
    }
  };

  function escapeHTML(txt) {
      return txt.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
  }

  function addUser(name, image, id) {
    updateConnectedUsers({ distName: name, avatarURL: image, clientID: id});
  }

  function removeUser(id) {
    updateConnectedUsers(id, 'remove');
  }

  function updateConnectedUsers(userInfo, type) {
    if(type === 'add') {
      var userElement;
      userElement = $('<div class="who-is-online-element" id="' + userInfo.clientID + '"' + ' ></div>');
      userElement.append($('<img src="' + userInfo.avatarURL + '" class="user-image">'));
      userElement.append($('<h1 class="user-name">'+ userInfo.distName + '</h1>'));
      userList.append(userElement);
    } else if (type === 'remove') {
      var element = document.getElementById(userInfo);
      element.parentNode.removeChild(element);
    }
  }

  function appendMessage (type, message, name, timestamp) {
    // NOT DRY AT ALL.
    var chatBubble;
    if (type === 'message') {
      chatbubble = $('<div class="chat-body-element-message"></div>');
      chatbubble.append($('<img src="' + avatar_url + '" class="user-image"</img>'));
      chatbubble.append($('<h1 class="user-name">' + name + '</h1>'));
      chatbubble.append($('<p class="message">' + escapeHTML(message) + '</p>'));
      chatbubble.append($('<p class="timestamp">' + timestamp  + '</p>'));
      chat.append(chatbubble);
    } else if (type === 'history') {
      if (message.type === 'connection') {
        chatbubble = $('<div class="chat-body-element-movement"></div>');
        chatbubble.append($('<img src="' + avatar_url + '" class="user-image"</img>'));
        chatbubble.append($('<p class="message">' + message.ms + '</p>'));
        chatbubble.append($('<p class="timestamp">' + message.ts + '</p>'));
        chat.append(chatbubble);
      } else {
        chatbubble = $('<div class="chat-body-element-message"></div>');
        chatbubble.append($('<img src="' + avatar_url + '" class="user-image"</img>'));
        chatbubble.append($('<h1 class="user-name">' + name + '</h1>'));
        chatbubble.append($('<p class="message">' + escapeHTML(message.ms) + '</p>'));
        chatbubble.append($('<p class="timestamp">' + message.ts + '</p>'));
        chat.append(chatbubble);
      }
    } else if (type === 'history') {
      chatbubble = $('<div class="chat-body-element-message"></div>');
      chatbubble.append($('<img src="' + avatar_url + '" class="user-image"</img>'));
      chatbubble.append($('<h1 class="user-name">' + name + '</h1>'));
      chatbubble.append($('<p class="message">' + escapeHTML(message.ms) + '</p>'));
      chatbubble.append($('<p class="timestamp">' + message.ts + '</p>'));
      chat.append(chatbubble);
    } else if (type === 'connected') {
      chatbubble = $('<div class="chat-body-element-movement"></div>');
      chatbubble.append($('<img src="' + avatar_url + '" class="user-image"</img>' ));
      chatbubble.append($('<p class="message">' + message + ' has connected</p>'));
      chatbubble.append($('<p class="timestamp">' + timestamp + '</p>'));
      chat.append(chatbubble);
    } else if (type === 'disconnected') {
      chatbubble = $('<div class="chat-body-element-movement"></div>');
      chatbubble.append($('<img src="' +
        avatar_url +
        '" class="user-image"</img>'
      ));
      chatbubble.append($('<p class="message">' + message + ' has disconnected</p>'));
      chatbubble.append($('<p class="timestamp">' + timestamp + '</p>'));
      chat.append(chatbubble);
    }
    $('#chat-body').animate({ scrollTop: $('#chat-body')[0].scrollHeight }, "slow");
  }
});
